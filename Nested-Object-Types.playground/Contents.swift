import UIKit

//Nested Object Types
//An object type may be declared inside an object type declaration, forming a nested type
/*the surrounding object type acts as a namespace, and must be referred to
explicitly in order to access the nested object type:

Dog.Noise.noise = "arf"*/

class Dog {
    struct Noise {
        static var noise = "woof"
    }
    func bark() {
        print(Dog.Noise.noise)
    }
}

var d = Dog()
d.bark()
Dog.Noise.noise = "grrr"
print(Dog.Noise.noise)

