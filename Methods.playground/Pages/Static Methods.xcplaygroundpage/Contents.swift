//: [Previous](@previous)

import Foundation

//static/class methods
/*A static/class method is accessed through the type.
Within the body ofa static/class method, self means the type
 
A static/class method can’t refer to “the instance” because there is no instance;
thus, a static/class method cannot directly refer to any instance properties or
call any instance methods.*/
 
struct Greeting {
    static let firendly = "Hello there"
    
    static func beFriendly() {
        print(self.firendly)
    }
}

print(Greeting.beFriendly())


/*An instance method, can refer to the type, and can thus access static/class properties
and can call static/class methods. */

class Dog {
    static var whatDogSay = "Grr"
        
    func bark() {
        print(Dog.whatDogSay)
    }
}

let fido = Dog()
//fido.bark()  //grr



//: [Next](@next)
