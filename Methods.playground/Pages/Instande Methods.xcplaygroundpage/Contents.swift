//: [Previous](@previous)

import Foundation

//Methods
/*A method is a function, by default a method is an instance method.
This means that it can be accessed only through an instance. Within the body
of an instance method, self is the instance.*/

class Dog {
    var name: String
    var license: Int
    var whatDogSay = "woof"
    
    init(name: String, license: Int) {
        self.name = name
        self.license = license
    }
    
    func bark() {
        print(self.whatDogSay)
    }
    
    func speak() {
        self.bark()
        print("I'm\(self.name)")
    }
}
/*the speak method calls the instance method bark by way of self, and obtains
 the value of the instance property name by way of self; and the bark instance method
 obtains the value of the instance property whatDogsSay by way of self 
instance code can use self to refer to this instance.*/

let doggy = Dog(name: "Doggy", license: 343434)
print(doggy.speak())


/*Even though store is an instance method, we are able to call it as a class method
with a parameter that is an instance of this class! The reason is that an instance method
is actually a curried static/class method composed of two functions,
one function that takes an instance, and another function that takes the parameters
of the instance method. After that code, f is the second of those functions,
and can be called as a way of passing a parameter to the store method of the instance m */

class MyClass {
    var s = ""
    
    func store(_ s: String) {
        self.s = s
    }
}

let myClassy = MyClass()
let f = MyClass.store(myClassy)
f("func that takes the parameter of the instance method")
print(myClassy.s)

//: [Next](@next)
