import Foundation

/*A computed variable can’t have setter observers.*/

var now: String {
    get {
        Date().description
    }
    
    set {
        print(newValue)
    }
}

now = "Howdy"
print(now)

/*There doesn’t have to be a setter. If the setter is omitted,
 this becomes a read-only variable. This is the computed variable equivalent of a let variable:
 attempting to assign to it is a compile error. */

/*There must always be a getter! However, if there is no setter,
 the word get and the curly braces that follow it can be omitted.
 This is a legal declaration of a read-only variable (omitting the return keyword):*/

var today: String {
    Date().description
}





