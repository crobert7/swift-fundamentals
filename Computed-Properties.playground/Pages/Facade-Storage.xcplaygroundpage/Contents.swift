//: [Previous](@previous)

import Foundation

//Facade Storage

/*A computed variable can sit in front of one or more stored variables, acting
as a gatekeeper on how does stored variabales are set and fetched */

//Commonly a public computed variable is backed by a private stored variable.

//The simplest possible storage facade would do no more than get and set the private stored variable directly

private var _p: String = ""

var p: String {
    get {
//        self._p
        _p
    }
    set {
        _p = newValue
    }
}

p = "Hello Rob"
print(_p)


//Clamped Setter
/*computed instance property getter or setter can refer to other instance members. because in
 general the initializer for a stored property can’t do that. The reason it’s legal for
 a computed property is that the getter and setter functions won’t be called until the instance actually exists.*/

private var _pp: Int = 0

var pp: Int {
    get {
        _pp
    }
    set {
        _pp = max(min(newValue, 5),0)
    }
}

pp = 7
print(_pp)




//: [Next](@next)
