//: [Previous](@previous)

import Foundation

//A stuct can have instance and static methods, including, subscripts

/*If an instance method sets a property, it must be marked as mutating and the caller's
reference to the struct unstance mus be a variable(var) not a constant(let)*/

struct Digit {
    private var number: Int
    init(_ n: Int) {
        self.number = n
    }
    
    mutating func changeNumberTo(_ n: Int) {
        self.number = n
        //or self = Digit(n)
    }
}

var d = Digit(23)
d.changeNumberTo(7)

/*A subscript or computed property setter is considered mutating by default and does
 not have to be specially marked. However, if a getter sets another property as a side
 effect, it must be marked mutating get
 
A mutating instance method can replace this instance with another instance, by setting
self to a different instance of the same struct */




//: [Next](@next)
