import Foundation

//Structs

struct Digit {
    var number = 42
}

/*By default structs has an implicit initializer with no parameters init().
This is called memeberwise initialiazer*/

let digit = Digit()

//If you add an explicit initializer, you lose the implicit initializer

struct Diggit {
    var num = 42
    init(){}
    init(num: Int) {
        self.num = num
    }
}
/*Now you can say Diggit(num: 42), but you can't say Diggit(), of course you can add
 an initilizaer that does the same thing init() */
let dig = Diggit()
let d = Diggit(num: 42)

struct Test {
    var number = 23
    var name: String
    let age: Int
    let greeting = "Hello"
}
//Now we have to ways to create a Test instance
let t1 = Test(number: 23, name: "rob", age: 27)
let t2 = Test(name: "rob", age: 27)
let t3 = Test(number: 34, name: "checo", age: 28)
/*The memberberwise initializer  includes number, name and age, but not greeting
 because greeting is a let property thathas been initialiazed, before the initializer
 is called, number hsa been initialized too, but it is a var property, so the
 memberwise initializer includes it, but you can also omit it from yourcall to the
 memberwise initializer, because it has been initialized already */








