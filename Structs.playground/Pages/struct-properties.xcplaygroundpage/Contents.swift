//: [Previous](@previous)

import Foundation

//Struct Properties
/*A struct can have instance properties and static properties, which can be
stored or computed variables.*/

struct Digit {
    var number: Int
    init(_ n: Int) {
        self.number = n
    }
}

/*If othercode wants to set a property of a struct instance, its reference to that
 instance must be a variable (var), not a constant (let)*/

var d = Digit(23)
d.number = 7


//: [Next](@next)
