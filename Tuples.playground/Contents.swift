import Foundation

//A tuple is a lightweight custom ordered collection of multiple values
var pair: (Int, String) = (1, "two")

let index: Int
let s: String
(index, s) = (1, "two")
//To ignore one of the assigned values, use an underscore to represent it in the receiving tuple
let(_, s1) = pair

let s2 = "hello"
for (ix, char) in s2.enumerated() {
    print("index:\(ix), character:\(char)")
}

for t in s2.enumerated() {
    print("character \(t.offset) is \(t.element)")
}


//we can refer to the individual elements of a tuple directly
var tup = (2, "hola")
let ix = tup.0
tup.0 = 5

//or adding a label
var pairee: (first: Int, second: Int) = (0,0)
var par = (first: 1, second: "two")
par.first = 7
par.second = "nine"

func tupleMaker() -> (first:Int, second:String) {
    return (1, "Two") // no labels here
}
let maker = tupleMaker().first // 1


