//: [Previous](@previous)

import Foundation

//Enums
/*An enum is an object type whose instances represent distinct predefined alternative values*/

enum Filter {
    case albums
    case books
    case podcasts
    case playlists
}

func filterExpected(_ type: Filter) {
    if type == .books {
        print("It's a book")
    }
}

//instance of Filter representing the albums case
let type = Filter.albums

let type1: Filter = .playlists

filterExpected(.books) // It's a book
