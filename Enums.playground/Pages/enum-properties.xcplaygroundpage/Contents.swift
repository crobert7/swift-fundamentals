//: [Previous](@previous)

import Foundation

//Enums properties

/*An enum can have instance properties and static properties,
but there’s a limitation: an enum instance property can’t be a stored property.*/

enum Filter: String {
    case albums = "Albums"
    case playlists = "Playlists"
    case podcasts = "Podcats"
    case books = "Books"
    
    var query: MPMediaQuery {
        switch self {
        case .albums:
            return .albums()
        case .playlists:
            return .playlits()
        case .podcasts:
            return .podcasts()
        case .books:
            return .books()
        }
    }
}

/*If an enum instance property is a computed variable with a setter,
other code can assign to this property. However, that code’s reference
to the enum instance itself must be a variable (var), not a constant (let)*/

enum Silly {
    case one
    var sillyProperty: String {
        get { "Howdy" }
        set {  }
    }
}

var silly: Silly = Silly.one

/*But if silly were declared with let instead of var,
trying to set silly.sillyProperty would cause a compile error.*/
silly.sillyProperty = "Voilà"

/*An enum static property can have a property wrapper, but an enum
instance property can’t, because that would imply storage of an instance
of the underlying @propertyWrapper type.*/

//: [Next](@next)
