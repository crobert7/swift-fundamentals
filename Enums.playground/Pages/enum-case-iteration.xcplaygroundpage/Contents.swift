//: [Previous](@previous)

import Foundation

/*It is often useful to have a list — that is, an array of all the
cases of an enum. You could define this list manually as a static property of the enum */

enum Filter: String {
    case albums = "Albums"
    case playlists = "Playlists"
    case podcasts = "Podcasts"
    case books = "Books"
    static let cases: [Filter] = [.albums, .books, .playlists, .podcasts, .books]
}

/*the list of cases can be generated for you automatically.
Simply have your enum adopt the CaseIterable protocol
now the list of cases springs to life as a static property called allCases*/

enum Filters: String, CaseIterable {
    case albums = "Albums"
    case playlists = "Playlists"
    case podcasts = "Podcasts"
    case books = "Books"
//  allCases: [Filter] = [.albums, .books, .playlists, .podcasts, .books]
}

//: [Next](@next)
