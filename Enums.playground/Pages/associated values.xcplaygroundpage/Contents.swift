//: [Previous](@previous)

import Foundation

//associated values

//Is a way to construct a case whose constant value can be set when the instance is created.

/*your choice of type is not limited. Most often, a single value will be
attached to a case, so you’ll write parentheses containing a single type name.*/

enum MyError {
    case number(Int)
    case message(String)
    case fatal
}

/*That code means that, at instantiation time, a MyError instance with
the .number case must be assigned an Int value, a MyError instance with
the .message case must be assigned a String value, and a MyError instance with
the .fatal case can’t be assigned any value */

let err: MyError = MyError.number(4)

let num = 5
let error: MyError = .number(num)

enum Error {
    case number(n: Int)
    case message(s: String)
    case fatal(n: Int, s: String)
}

let err1: Error = .fatal(n: 12, s: "Fatal error")


/*By default, the == operator cannot be used to compare
 cases of an enum if any case of that enum has an associated value: */

//But if you declare this enum explicitly as adopting the Equatable protocol, the == operator starts working:

enum Error2: Equatable {
    case number(Int)
    case message(String)
}
func errorT(type: Error2) {
    if type == .number(7) {
        print("its a number")
    }
}









//: [Next](@next)
