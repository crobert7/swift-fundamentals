//: [Previous](@previous)

import Foundation

//An enum can have instance methods (including subscripts) and static methods.

/*enum Shape {
 case rectangle
 case ellipse
 case diamond
 func addShape (to p: CGMutablePath, in r: CGRect) -> () {
    switch self {
        case .rectangle:
        p.addRect(r)
    case .ellipse:
        p.addEllipse(in:r)
    case .diamond:
        p.move(to: CGPoint(x:r.minX, y:r.midY))
        p.addLine(to: CGPoint(x: r.midX, y: r.minY))
    p.addLine(to: CGPoint(x: r.maxX, y: r.midY))
    p.addLine(to: CGPoint(x: r.midX, y: r.maxY))
    p.closeSubpath()
    }
 }
}*/

/*An enum instance method that modifies the enum itself must be marked as mutating.
For example, an enum instance method might assign to an instance property of self;
even though this is a computed property, such assignment is illegal unless the method
is marked as mutating. The caller of a mutating instance method must have a variable
reference to the instance (var), not a constant reference (let).
A mutating enum instance method can replace this instance with another instance,
by assigning another case to self.*/

enum Filter: String, CaseIterable {
    case albums = "Albums"
    case playlists = "Playlists"
    case podcasts =  "Podcasts"
    case books = "Books"
    
    mutating func advance() {
        let cases = Filter.allCases
        var ix = cases.firstIndex(of: self)!
        ix = (ix + 1) % cases.count
        self = cases[ix]
    }
}

//Observe that type is declared with var; if it were declared with let, we’d get a compile error.
var type: Filter = .books
type.advance()

/*A subscript or computed property setter is considered mutating by default and
does not have to be specially marked.However,if agetter set sanother property as
a side effect,it must be marked mutating get.*/


//: [Next](@next)
