//: [Previous](@previous)

import Foundation

/*
An explicit enum initializer must do what default initialization does:
it must return a particular case of thisenum. To do so, set self to the case.
In this example, I’ll expand my Filter enum so that it can be initialized
with a numeric argument*/

enum Filter: String, CaseIterable {
    case albums = "Albums"
    case playlists = "Playlists"
    case podcasts = "Podcasts"
    case books = "Books"
    
    init(_ ix: Int) {
        self = Filter.allCases[ix]
    }
}

//Now there are three ways to make a Filter instance
let type: Filter = Filter.albums
let type2: Filter = Filter(rawValue: "Playlists")!
/*We’ll crash in the third line if the caller passes a number
that’s out of range (less than 0 or greater than 3)*/
let type3: Filter = Filter(2) //.podcasts

//To avoid the crash we need to initialize as failable

enum Filters: String, CaseIterable {
    case albums = "Albums"
    case playlists = "Playlists"
    case podcasts = "Podcasts"
    case books = "Books"
    
    init?(_ ix: Int) {
        if !Filter.allCases.indices.contains[ix] {
            return nil
        }
        self = Filter.allCases[ix]
    }
}

let typee: Filter = Filter.books
let typee1: Filter = Filter(rawValue: "Albums")
let typee2: Filter = Filter(1)



//: [Next](@next)
