//: [Previous](@previous)

import Foundation

//Inference of Type Name with Static/Class Members

struct Thing: RawRepresentable {
    let rawValue: Int
    static let one: Thing = Thing(rawValue: 1)
    static let two: Thing = Thing(rawValue: 2)
}

//we can refer to Thing.one as .one where a Thing instance is expected
let thing: Thing = Thing.one

//: [Next](@next)
