//: [Previous](@previous)

import Foundation

//Raw Values

//The values carried by the cases are called their raw values

/*Optionally, when you declare an enum, you can add a type declaration
The types attached to an enum in this way are limited to numbers and strings,
and the values assigned must be literals. */


/*If the type is an integer numeric type, the values can be implicitly
assigned, and will start at zero by default*/

enum PepBoy: Int {
    case manny   //many carries the value 0
    case moe
    case jack
}

/*If the type is String, the implicitly assigned values are the string equivalents of the case names */
enum Filter: String {
    case albums    // carries a value of "albums"
    case playlits
    case podcasts
    case books
}

let type: Filter = Filter.albums
print(type.rawValue)

//you can assign values explicitly as part of the case declarations

enum Normal : Double {
    case fahrenheit = 98.6
    case centigrade = 37
}
enum PepBoys : Int {
    case manny = 1
    case moe // 2 implicitly
    case jack = 4
}

// The raw value associated with each case must be unique within this enum; the compiler will enforce this rule

enum Filters: String {
    case albums = "Albums"
    case playlists = "Playlists"
    case podcasts = "Podcasts"
    case books = "Audiobooks"
}

//in particular, you can instantiate an enum that has raw values by using its init(rawValue:) initializer:
/*
However, the attempt to instantiate the enum in this way still might fail,
because you might supply a raw value corresponding to no case; therefore,
this is a failable initializer, and the value returned is an Optional.
In that code, type is not a Filter; it’s an Optional wrapping a Filter.*/

let typee = Filters(rawValue: "Albums")
print(typee == .playlists ? "It's album" : "It's not album")







//: [Next](@next)
