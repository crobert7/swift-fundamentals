import Foundation

//Enums
/*An enum is an object type whose instances represent distinct predefined alternative values.*/

enum Filter {
    case albums
    case playlists
    case podcasts
    case books
}

// instance of Filter representing the albums case

let type = Filter.albums

/*If the type is known in advance, you can omit the name of the enum;
the bare case must still be preceded by a dot */

let type1 : Filter = .albums

func filterExpected(_ type: Filter) {
    if type == .books {
        print("It's a book")
    }
}

filterExpected(.books) //It's a book







