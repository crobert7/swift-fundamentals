//: [Previous](@previous)

import Foundation

/*An initializer can return an Optional wrapping the new instance.
In this way, nil can be returned to signal failure. An initializer that
behaves this way is a failable initializer. */

class Dog {
    var name: String
    var license: Int
    
    init?(name: String, license: Int) {
        if name.isEmpty {
            return nil
        }
        license <= 0 {
            return nil
        }
        self.name = name
        self.license = license
    }
}

/*The resulting value is typed as an Optional wrapping a Dog,
and the caller will need to unwrap that Optional (if isn’t nil) before
sending any messages to it.*/

//: [Next](@next)
