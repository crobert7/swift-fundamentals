//: [Previous](@previous)

import Foundation

//An initializer that calls another initializer is called a delegating initializer.

/*When an initializer delegates, the other initializer — the one that it delegates to
— must completely initialize the instance first, and then the delegating initializer
 can work with the fully initialized instance*/

struct Digit {
    var number: Int
    var meaningOfLife: Bool
    
    init(number: Int) {
        self.number = number
        self.meaningOfLife = false
    }
    
    init() { //This is the delegating initializer
        self.init(number: 42)
        self.meaningOfLife = true
    }
}
//A delegating initializer cannot set a constant propertyt hat is because
//it cannot refer to the property until after it has called the other initializer,
//and at that point the instance is fully formed

//: [Next](@next)
