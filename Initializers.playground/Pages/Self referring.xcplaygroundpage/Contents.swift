//: [Previous](@previous)

import Foundation

//Self
/*An initializer may refer to an already initialized instance property,
and may refer to an uninitialized instance property in order to initialize it.
Otherwise, an initializer may not refer to self, explicitly or implicitly,
until all instance properties have been initialized.*/

struct Cat {
    var name: String
    var license: Int
    
    init(name: String, license: Int) {
        self.name = name
        //meow() too soon-compile error
        self.license = license
    }
    
    func meow() {
        print("Meow")
    }
}




//: [Next](@next)
