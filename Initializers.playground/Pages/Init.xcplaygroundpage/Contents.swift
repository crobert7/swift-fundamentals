import Foundation

class Dog {
    var name: String = ""
    var license: Int = 0
    
    init() {
        
    }
    
    init(name: String) {
        self.name = name
    }
    
    init(license: Int) {
        self.license = license
    }
    
    init(name: String, license: Int) {
        self.name = name
        self.license = license
    }
}

let dog: Dog = Dog()
let scooby: Dog = Dog(name: "scooby")
let scott: Dog = Dog(license: 234432)
let tommy: Dog = Dog(name: "Tommy", license: 324134)


class Doggy {
    var name: String
    var license: Int
    
    init(name: String = "", license: Int = 0) {
        self.name = name
        self.license = license
    }
}

/*That code is legal (and common) — because an initializer initializes! In other words
 I don’t have to give my properties initial values in their declarations, provided I give
 them initial values in all initializers. That way, I am guaranteed that all my instance
 properties have values when the instance comes into existence*/

let doggy: Dog = Dog()
let blacky: Dog = Dog(name: "blacky")
let pongo: Dog = Dog(license: 342344)
let kirsy: Dog = Dog(name: "kirsy", license: 45434345)


/*we have been very generous with our initializers: we are letting the caller instantiate a Cat
without supplying a name: argument or a license: argument. Usually, however, the
purpose of an initializer is just the opposite: we want to force the caller to supply
all needed information at instantiation time. In real life, it is much more likely
that our Cat class would look like this:*/

class Cat {
    let name : String
    let license : Int
    
    init(name:String = "", license:Int = 0) {
        self.name = name
        self.license = license
    }
}

let catty: Cat = Cat()
/*our Cat has a name property and a license property, and values for these
 must be supplied at instantiation time (there are no default values), and
 those values can never be changed thereafter (the properties are constants).
 In this way, we enforce a rule that every Dog must have a meaningful name and license.
 There is now only one way to make a Dog:*/
let kitty: Cat = Cat(name: "kitty", license: 321414)



