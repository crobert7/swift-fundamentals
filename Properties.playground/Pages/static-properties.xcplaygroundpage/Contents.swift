import Foundation

//Static class properties

/*static/class property, on the other hand, is accessed through the type,
and is scoped to the type, which usually means that it is global and unique.*/

struct Greeting {
    static let friendly: String = "hello there"
    static let hostile: String = "go away"
}

print(Greeting.friendly)
print(Greeting.hostile)

//static properties can be initialized with reference to one another;
//the reason is that static property initializers are lazy
struct Greets {
    static let friendly = "hello there"
    static let hostile = "go away"
    static let ambivalent = friendly + " but " + hostile
//    Anotherway
    static let ambi = Greets.friendly + " but " + hostile
//   If I declare as a computed property I can use self
    static var both : String {
        self.friendly + " but " + self.hostile
    }
}





