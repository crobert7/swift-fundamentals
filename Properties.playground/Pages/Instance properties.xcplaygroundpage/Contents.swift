//: [Previous](@previous)

import Foundation

//A property is a variable

class Dog {
    var name: String
    var license: Int
    
    init(name: String, license: Int) {
        self.name = name
        self.license = license
    }
}

//How to access properties

/*If a property is an instance property (the default), it can be accessed only
through an instance, and its value is separate for each instance. */

let fido: Dog = Dog(name: "fido", license: 12334)
let scooby: Dog = Dog(name: "scooby", license:33432)
let aName = fido.name
let anotherName = scooby.name

//: [Next](@next)
