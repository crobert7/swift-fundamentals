//: [Previous](@previous)

import Foundation

/*A property declaration that assigns an initial value to the property
cannot fetch an instance property or call an instance method.
Such behavior would require a reference, explicit or implicit, to self;
and during initialization, there is no self yet */

//Here we have two options deeclare as a computer property

class John {
    let name = "John"
    let lastName = "Doe"
//    let whole = self.name + " " + self.lastName compile error
    var whole: String {
        self.name + " " + self.lastName
    }
}
let j: John = John()
print(j.whole)

//The second is to declare as a lazy var

class Matt {
    let first = "Matt"
    let last = "Numberg"
    lazy var whole = self.first + " " + self.last
}

let m: Matt = Matt()
print(m.whole)

//Also can be initialized as part o its declaration define-call

class Jim {
    var first = "Jim"
    var last = "Doug"
    
    lazy var whole: String = {
        var s = self.first
        s.append(" ")
        s.append(self.last)
        return s
    }()
}


//: [Next](@next)
