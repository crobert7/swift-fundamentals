import Foundation

/*A subscript is a method that is called by appending square brackets containing
arguments directly to a reference. You can use this feature for whatever you like,
but it is suitable particularly for situations where this is an object type with
elements that can be appropriately accessed by key or by index number.*/

struct Digit {
    var number: Int
    init(_ n: Int){
        self.number = n
    }
    subscript(ix: Int) -> Int {
        get {
            let s = String(self.number)
            return Int(String(s[s.index(s.startIndex, offsetBy:ix)]))!
        }
        set {
            var s = String(self.number)
            let i = s.index(s.startIndex, offsetBy: ix)
            s.replaceSubrange(i...i, with: String(newValue))
            self.number = Int(s)!
        }
    }
}

//getter reed-only property
var digit = Digit(793)
let aDigit = digit[1]

//setter
/*the instance with appended square brackets containing the
arguments is used just as if you were setting a property value*/

var d = Digit(1234)
d[0] = 9

//An object type can declare multiple subscript methods,
//distinguished by their parameters. New in Swift 5.1, a subscript can be a static/class method.

