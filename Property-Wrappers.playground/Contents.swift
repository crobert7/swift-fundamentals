import Foundation

//Property Warappers
//property-wrapped computed variable can have setter observers

/*A property wrapper is declared as a type marked with the @propertyWrapper attribute, and must have a
wrappedValue computed property*/
class Clamp {

@propertyWrapper struct Clamped {
    
    var _i: Int = 0
    
    var wrappedValue: Int {
        get {
            self._i
        }
        set {
            self._i = Swift.max(Swift.min(newValue, 5), 0)
        }
    }
    
}
/*we can declare a computed property marked with a custom attribute whose name is the same as that struct (@Clamped), with no getter or setter*/
    
///This is an insntance of the Clamped struct
@Clamped var p: Int
/*Our property p doesn’t need to be initialized, because it’s a computed property.
And it doesn’t need a getter or a setter, because the wrappedValue
computed property of the Clamped struct supplies them*/

    func getP() {
        print(self.p)
    }

}

/*When we set self.p to some value through assignment, the assignment passes
 through the Clamped instance’s wrappedValue setter, and the resulting clamped value
 is stored in the Clamped instance’s _i property. When we fetch the value of self.p,
 what we get is the value returned from the Clamped instance’s wrappedValue getter,
 which is the value stored in the Clamped instance’s _i property.*/

let c: Clamp = Clamp()
c.p = 7
print(c.p)
//This is the value of the instance property _i
c.getP()

