import UIKit

//Optional Chain

var stringMaybe : String?
stringMaybe = "howdy"
//After that code, upper is not a String; it is not "HOWDY". It is an Optional wrapping "HOWDY".
let upper = stringMaybe?.uppercased()

class Dog {
    var noise: String?
    
    func bark() -> String? {
        self.noise
    }
}

let doggy = Dog()
doggy.bark()?.uppercased()

/*If you want to preserve the optionality Swfit provides two methods map(_:) & flatmap(_:)
these are methods of Optional itself, so it’s fine to send them to an Optional. The parameter
is a function that you supply (usually as an anonymous function) that takes whatever type is
wrapped in the Optional; the unwrapped value is passed to this function, and now you can send
a message to it. The result of the function is then wrapped as an Optional*/
let s2 = stringMaybe.map{ $0.uppercased() }

/*The output Optional type doesn’t have to be the same as the input Optional type.
 Indeed, it commonly is not; map(_:) and flatMap(_:) are often used when the goal is to coerce
 but the coercion fails because the string wrapped by stringMaybe doesn’t represent an integer.
*/
//let integer = stringMaybe.flatmap { Int($0) }
