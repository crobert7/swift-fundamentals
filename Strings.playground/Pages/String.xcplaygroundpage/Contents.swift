import UIKit

let string1 = "Hello"
let string2 = "World"
let space = " "
let greet = string1 + string2

var string = "hello"
string.append(string2)
let greets = [string1,string2].joined(separator: space)
greet.count
greet.capitalized
greet.lowercased()
String(greet.reversed())

for char in string2 {
    print(char)
}

//Raw string literal
//The downside is that if you want to use a backslash as an escape character in a raw string literal, you must follow it with the same number of # characters you are using to surround your literal
let pattern = #"\b\d\d\b"#
print(pattern)
let patt = ##"b\b\b\b"##
print(patt)

//You can decompose you can decompose a String into its UTF-8 codepoints or its UTF-16 codepoints, using the utf8 and utf16 properties:
let s = "\u{BF}Qui\u{E9}n?"
for i in s.utf8 {
    print(i) // 194, 191, 81, 117, 105, 195, 169, 110, 63
}

for i in s.utf16 {
    print(i)
}

/*The differnce of a character and the NSString is that Swift involves a string by character and NSString
 conception is  UTF-16 codepoints*/
let s1 = "Ha\u{030A}kon"
print(s1.count) // 5
let length = (s1 as NSString).length // or: s.utf16.count print(length) // 6

