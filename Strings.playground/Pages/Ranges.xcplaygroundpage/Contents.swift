//: [Previous](@previous)

import Foundation

//A range represents a pair of endpoints.
//Ranges can't be in reverse, if you want that you need to use the method reversed

for ix in (1...3).reversed() {
    print(ix)
}

//A common use of a Range is to index into a sequence
//a way to get the second, third, fourth character of the string
let s = "hello"
let arr = Array(s)
let result = arr[1...3]
let s2 = String(result)

//The same as above but with index
let ix1 = s.index(s.startIndex, offsetBy:1)
let ix2 = s.index(ix1, offsetBy:2)
let s3 = s[ix1...ix2]



//: [Next](@next)
