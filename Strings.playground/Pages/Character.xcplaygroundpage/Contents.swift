//: [Previous](@previous)

import Foundation
//A string is a sequence of characters, Strings is both a sequence of character and a collection of characters.
//Sequence and Collections are protocols
let string = "Robert"
for i in string {
    print(i)
}

let c: Character = Character("c")
print(c)
//We can initialize a string from a character
let s = String(c).uppercased()
var s1 = "hello"
s1.first
s1.last
let fistL = s1.firstIndex(of: "l")
let firstSmall = s1.firstIndex{ $0 < "f" }
print(firstSmall ?? "no value")
s1.contains("o")
s1.contains{ "xyz".contains($0) }
s1.filter{ "aeiou".contains($0) }
s1.dropFirst()
//To pass a Substring where a String is expected, coerce the Substring to a String explicitly
let s2 = s1.dropFirst()
s1 = String(s2)
s1.suffix(4)
print(s1)


//The split breaks a string up into an array
let s3 = "hello world"
let arr = s3.split{$0 == " "}.map{String($0)}
print(arr)

//Get the index of a string, we always need an startIndex
let indexString = "Rocio"
let ix = indexString.startIndex
let x2 = indexString.index(ix, offsetBy: 1)
let substring = indexString[x2]
let char = indexString[indexString.index(after: ix)]

var cad = "looper"
var i = cad.index(cad.startIndex, offsetBy: 1)
cad.insert(contentsOf: "ey, l", at: i)
print(cad)









//: [Next](@next)
